<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iaaimodel extends CI_Model {
	function count_all()
	{
		$this->db->from('response_iaai');
		return $this->db->count_all_results();
	}
	
	function insert_item($item)
	{
		$this->db->insert('response_iaai', $item);
	}
	
	function update_item($item)
	{
		$this->db->where('lot_number', $item['lot_number']);
		$this->db->update('response_iaai', $item);
	}
	
	function exists_lot($lot_numner)
	{
		$this->db->where('lot_number', $lot_numner);
		$this->db->from('response_iaai');
		
		return $this->db->count_all_results();
	}
	
	function get_cars_urls_for_year($year)
	{
		$this->db->select('lot_url');
		$this->db->where('year', $year);
		
		$query = $this->db->get('response_iaai');
		return $query->result_array();
	}
	
	function get_cars_lots_for_year($year)
	{
		$this->db->select('id,lot_number');
		$this->db->where('year', $year);
		
		$query = $this->db->get('response_iaai');
		return $query->result_array();
	}
	
	function insert_img_item($item)
	{
		$this->db->insert('response_iaai_images', $item);
	}
	
	function delete_img($id)
	{
		$this->db->where('res_id', $id);
		$this->db->delete('response_iaai_images');
	}
}