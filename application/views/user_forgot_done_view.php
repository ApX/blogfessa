<?php

$header['title'] = "Email Sent";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">E-mail Confirmation</h1>
        <p class="lead blog-description">E-mail was sent. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
		<table><tr><td width="15%">&nbsp;</td><td>
		  <blockquote>
				<table>
				<tr><td>E-mail was sent to <a href="mailto://<?=$user['email']?>"><?=$user['email'];?></a>.</td></tr>
				<tr><td>The password for user <u><?=$user['username'];?></u> is attached.</td></tr>
				</table>
		  </blockquote></td></tr></table>
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
