<?php

$header['title'] = "User Forgot Credentials";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}
	

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">Forgotten Password Form</h1>
        <p class="lead blog-description">Please, provide your username. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
		<?php echo validation_errors(); ?>
		<?php echo form_open('users/user_forgot'); ?>
			<table border="0">
			<tr><td>User&nbsp;</td><td><input type="text" name="username" id="username"></input></td></tr>
			<tr><td>&nbsp;</td><td><input type="submit" value="Email Password!"></input></td></tr>
			</table>
		</form>

      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	<script>
		$("#username").focus();
	</script>
  </body>
</html>
