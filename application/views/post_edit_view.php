<?php

$header['title'] = "Edit Post";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">Edit Post Form</h1>
        <p class="lead blog-description">Add new post. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
		<?php if($uservalid) : ?>
		<?php echo validation_errors(); ?>
		<?php echo form_open('posts/edit_post/'.$postid); ?>
			<table border="0" width="90%">
			<tr><td width="3%">&nbsp;</td><td colspan="2"><h2 class="blog-post-title"><?=$category_name?> Category</h2></td></tr>
			<tr><td>&nbsp;Title</td><td><input type="text" name="title" id="title" size="81" value="<?=$post_title;?>"></input></td></tr>
			<tr><td>&nbsp;</td><td><textarea rows="9" cols="40" name="body"><?=$post_body;?></textarea></td></tr>
			<tr><td>&nbsp;</td><td><input type="submit" value="Post!"></input></td></tr>
			</table>
		</form>
		<?php else : ?>
		<table><tr><td width="15%">&nbsp;</td><td>
		  <blockquote>
				<table>
				<tr><td>You are not logged in.</td></tr>
				<tr><td>To authenticate use <a href="<?=base_url();?>users/user_login">Login</a>.</td></tr>
				</table>
		  </blockquote></td></tr></table>
		<?php endif;?>
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	<script>
		$('#title').focus();
	</script>
  </body>
</html>
