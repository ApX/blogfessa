<?php

$header['title'] = $category_name." Category for Approval";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="row">
        <div class="col-sm-8 blog-main">

          <div class="blog-header">
			<table></tr>
            <td><h1 class="blog-title"><?=$category_name;?> Category for Approval</h1></td>
			<td>&nbsp;</td>
			<td valign="bottom"><h6>[<?=count($category_posts);?> post(s)] <a href="<?=base_url().'posts/new_post/'.$category_id;?>">new post</a></h6></td>
			</tr></table>
            <p class="blog-post-meta">Last <a href="<?=base_url().''.$last_update_url;?>">updated</a> by <a href="<?=base_url().'users/user_show/'.$last_update_user;?>"><?=$last_update_user;?></a> on <?=$last_update_time;?></p>
			
			
				<?php foreach($category_posts as $post):?>
					<pre><?php if($post['approved'] == 0) : ?><table border="0" width="100%"><tr><td style="color:red" align="right" valign="top" id="status">not approved...</td></tr></table><?php endif;?><font size="5"><?=$post['title'];?></font>
					<table border="0" width="100%">
						<tr><td width="2%">&nbsp;</td>
							<td width="30%"><code>by <a href="<?=base_url().'users/user_show/'.$post['author'];?>"><?=$post['author'];?></a></code></td>
							<td width="30%"><code>on <?=$post['date']?></code></td>
							<td><code><a href="<?=base_url().'posts/show_post/'.$post['id'];?>">read</a></code></td>
							<?php if($uservalid) :?>
							<?php if(($usersession == $post['author']) or ($usersession == 'admin')) : ?>
							<td><code><a href="<?=base_url().'posts/edit_post/'.$post['id'];?>">edit</a></code></td>
							<td><code><a href="<?=base_url().'posts/delete_post/'.$post['id'];?>">delete</a></code></td>
							<?php endif;?>
							<?php endif;?>
							<?php if($uservalid) :?>
							<?php if($usersession == 'admin' and $post['approved'] == 0) : ?>
							<td><code><a href="<?=base_url().'posts/approve_post/'.$post['id'];?>">approve</a></code></td>
							<?php endif;?>
							<?php endif;?>
						</tr></table></pre>
				<?php endforeach;?>

        </div><!-- /.blog-main -->
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
