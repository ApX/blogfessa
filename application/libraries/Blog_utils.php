<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Blog_utils {
	public function get_session_user($data)
	{
		$CI =& get_instance();
		
		$CI->load->library(array('session'));
		$session = $CI->session->all_userdata();
		
		if (isset($session['logged_in']))
		{
			$data['uservalid'] = TRUE;
			$data['usersession'] = $session['username'];
		}
		else
		{
			$data['uservalid'] = 0;
		}
		
		return $data;
	}
}
