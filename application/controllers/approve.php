<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approve extends CI_Controller {
	public function index()
	{
		$this->load->model('pmodel');
		$data['c_by_posts'] = $this->pmodel->categories_by_posts_for_approval();
		
		$this->load->model('../controllers/users');
		$data['uservalid'] = FALSE;
		$data = $this->users->get_session_user($data);
		
		$data['last_update_time'] = date('d-M-Y h:i:s a', time());
		$data['last_update_user'] = "aar";
		$data['last_update_url'] = "approve";
		
		$this->load->view('approve_view', $data);
	}
}
