<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umodel extends CI_Model {
	function user_new($data)
	{
		$this->db->insert('users', $data);
	}
	
	function user_get($user)
	{
		$this->db->where('username', $user);
		$query = $this->db->get('users');
		
		return $query->row_array();
	}
	
	function user_update($user,$data1)
	{
		$this->db->where('username', $user);
		$this->db->update('users', $data1); 
	}
}
