<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function user_new()
	{
		$this->load->helper(array('form', 'url', 'captcha', 'string'));
		$this->load->library(array('form_validation', 'session'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');
		
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric|max_length[20]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[20]');
		$this->form_validation->set_rules('age', 'Age', 'is_natural_no_zero|less_than[100]');
		$this->form_validation->set_rules('email', 'E-mail', 'required|max_length[254]|valid_email');
		$this->form_validation->set_rules('about', 'About', 'max_length[512]');
		
		$this->form_validation->set_rules('captcha','Captcha','callback_validate_captcha');
		
	
		if ($this->form_validation->run() == TRUE)
		{
			$this->form_validation->unset_field_data();
			
			$this->session->unset_userdata('captcha');
			$this->session->unset_userdata('image');
			
			$this->load->model('umodel');
			
			$data['username'] = $this->input->post('username');
			$data['password'] = $this->input->post('password');
			$data['age'] = $this->input->post('age');
			$data['email'] = $this->input->post('email');
			$data['about'] = $this->input->post('about');
			
			$this->umodel->user_new($data);
			
			$data['uservalid'] = FALSE;
			$data1 = $this->get_session_user($data);
			
			$this->load->view('user_added_view', $data1);
		}
		else
		{
			$random_numeric = random_string('numeric', 4);
			
			$vals = array(
				'word'	=> $random_numeric,
				'img_path'	=> './images/captcha/',
				'img_url'	=> base_url().'images/captcha/',
				'font_path'	=> './system/fonts/texb.ttf',
				'img_width'	=> '150',
				'img_height' => 30,
				'expiration' => 7200
				);

			$cap = create_captcha($vals);
			$data['image'] = $cap['image'];
			
			$this->session->set_userdata(array('captcha'=>$random_numeric, 'image' => $cap['time'].'.jpg'));
			

			$data['uservalid'] = FALSE;
			$data1 = $this->get_session_user($data);
			
			$this->load->view('user_new_view', $data1);
		}
	}
	
	public function user_login()
	{
		$this->load->helper(array('form', 'url', 'captcha', 'string'));
		$this->load->library(array('form_validation', 'session'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');
		
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_creds');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
	
		if ($this->form_validation->run() == TRUE)
		{
			$data['username'] = $this->input->post('username');
			
			$this->form_validation->unset_field_data();
			
			$newdata = array(
                   'username'  => $data['username'],
                   'logged_in' => TRUE
               );
			$this->session->set_userdata($newdata);
			
			redirect('/home');
		}
		else
		{
			$this->session->sess_destroy();
			$this->load->view('user_login_view');
		}
	}
	
	public function user_logout()
	{
		$this->load->library(array('form_validation', 'session'));
		
		$this->session->sess_destroy();
		$this->load->view('user_login_view');
	}
	
	public function user_preferences()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('form_validation'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');
		
		$this->form_validation->set_rules('age', 'Age', 'is_natural_no_zero|less_than[100]');
		$this->form_validation->set_rules('about', 'About', 'max_length[512]');
		$this->form_validation->set_rules('email', 'E-mail', 'required|max_length[254]|valid_email');
		
		$data['uservalid'] = FALSE;
		$data = $this->get_session_user($data);
		
		if($data['uservalid'])
		{
			$this->load->model('umodel');
			$data = $this->umodel->user_get($data['usersession']);
			
			$password = $data['password'];
			
			$data['uservalid'] = FALSE;
			$data = $this->get_session_user($data);
		}
		
		if ($this->form_validation->run() == TRUE)
		{
			$this->form_validation->unset_field_data();
			
			if($this->input->post('password'))
				$data['password'] = $this->input->post('password');
			else
				$data['password'] = $password;
			
			$data1['password'] = $data['password'];
			
			$data['age'] = $this->input->post('age');
			$data1['age'] = $data['age'];
			
			$data['email'] = $this->input->post('email');
			$data1['email'] = $data['email'];
			
			$data['about'] = $this->input->post('about');
			$data1['about'] = $data['about'];
			
			$this->load->model('umodel');
			$this->umodel->user_update($data['username'],$data1);
			
			$this->load->view('user_preferences_done_view', $data);
		}
		else
		{
			$this->load->view('user_preferences_view', $data);
		}
	}
	
	public function user_show($user)
	{
		$this->load->model('umodel');
		$data = $this->umodel->user_get($user);
		
		$data['uservalid'] = FALSE;
		$data = $this->get_session_user($data);

		$this->load->view('user_show_view', $data);
	}
	
	public function user_forgot()
	{
		$this->load->helper(array('form', 'url', 'captcha', 'string'));
		$this->load->library(array('form_validation', 'email'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');
		
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_user');
		
	
		if ($this->form_validation->run() == TRUE)
		{
			$this->form_validation->unset_field_data();
			
			$this->load->model('umodel');

			$data['user'] = $this->umodel->user_get($this->input->post('username'));
			
			$this->email->from('admin@blogfessa.3eeweb.com', 'Admin');
			$this->email->to($data['user']['email']);
			$this->email->subject('Forgotten Password for user '.$data['user']['username']);
			$this->email->message($data['user']['password']);
			$this->email->send();

			//echo $this->email->print_debugger();
			
			$this->load->view('user_forgot_done_view', $data);
		}
		else
		{
			$this->load->view('user_forgot_view');
		}
	}
	
	public function check_creds()
	{
		if (isset($_POST['username']))
		{
			$this->db->where('username', $this->input->post('username'));
			$this->db->where('password', $this->input->post('password'));
			
			$result = $this->db->count_all_results('users');
			
			if($result > 0)
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('check_creds', 'The username and password are wrong');
				return false;
			}
		}
	}
	
	public function check_user()
	{
		if (isset($_POST['username']))
		{
			$this->db->where('username', $this->input->post('username'));
			
			$result = $this->db->count_all_results('users');
			
			if($result > 0)
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('check_user', 'The username does not exist');
				return false;
			}
		}
	}
	
	public function validate_captcha()
	{
		if (isset($_POST['captcha']))
		if($this->input->post('captcha') != $this->session->userdata['captcha'])
		{
			$this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
			return false;
		}
		else
		{
			return true;
		}

	}
	
	public function get_session_user($data)
	{
		$this->load->library(array('session'));
		$session = $this->session->all_userdata();
		
		if (isset($session['logged_in']))
		{
			$data['uservalid'] = TRUE;
			$data['usersession'] = $session['username'];
		}
		else
		{
			$data['uservalid'] = 0;
		}
		
		return $data;
	}
	
}