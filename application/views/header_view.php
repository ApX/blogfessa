<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>bootstrap/css/blog.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>bootstrap/js/ie-emulation-modes-warning.js"></script>
	
	<script src="<?=base_url();?>jquery/jq.js"></script>
	
	<script src="<?=base_url();?>/tinymce/tinymce.min.js"></script>
	<script>
	tinymce.init({
		selector: "textarea",
		plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste jbimages"
  ],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
		
		relative_urls: true
	});
	</script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item <?php if($active_menu == 1) : echo "active";endif;?>" href="<?=base_url();?>home">Home</a>
          <a class="blog-nav-item <?php if($active_menu == 2) : echo "active";endif;?>" href="<?=base_url();?>users/user_new">New User</a>
		  <?php if($user_added == 1) : ?>
		  <a class="blog-nav-item active" href="#">User Added</a>
		  <?php endif;?>
		  <?php if($uservalid) : ?>
		  <a class="blog-nav-item <?php if($active_menu == 3) : echo "active";endif;?>" href="<?=base_url();?>users/user_logout">Logout(<?=$usersession;?>)</a>
		  <?php else : ?>
          <a class="blog-nav-item <?php if($active_menu == 3) : echo "active";endif;?>" href="<?=base_url();?>users/user_login">Login</a>
		  <?php endif;?>
		  <?php if($pref_updates == 1) : ?>
          <a class="blog-nav-item active" href="<?=base_url();?>users/user_preferences">Preferences(Updated)</a>
		  <?php else : ?>
          <a class="blog-nav-item <?php if($active_menu == 4) : echo "active";endif;?>" href="<?=base_url();?>users/user_preferences">Preferences</a>
		  <?php endif;?>
		  <?php if($uservalid) : ?>
		  <?php if($usersession == 'admin') : ?>
          <a class="blog-nav-item <?php if($active_menu == 5) : echo "active";endif;?>" href="<?=base_url();?>approve">Approve</a>
		  <?php endif;?>
		  <?php endif;?>
        </nav>
      </div>
    </div>