<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends CI_Controller {
	public function index()
	{
		redirect('/home');
	}

	public function show_category($category)
	{
		$this->load->model('pmodel');
		$data['category_posts'] = $this->pmodel->category_get($category);
		$data['category_name'] = $this->pmodel->category_get_name($category);
		$data['category_id'] = $category;
		
		$this->load->model('../controllers/users');
		$data['uservalid'] = FALSE;
		$data = $this->users->get_session_user($data);
		
		$data['last_update_time'] = date('d-M-Y h:i:s a', time());
		$data['last_update_user'] = "aar";
		$data['last_update_url'] = "home";
		
		$this->load->view('show_category_view', $data);
	}
	
	public function show_category_for_approval($category)
	{
		$this->load->model('pmodel');
		$data['category_posts'] = $this->pmodel->category_for_approval_get($category);
		$data['category_name'] = $this->pmodel->category_get_name($category);
		$data['category_id'] = $category;
		
		$this->load->model('../controllers/users');
		$data['uservalid'] = FALSE;
		$data = $this->users->get_session_user($data);
		
		$data['last_update_time'] = date('d-M-Y h:i:s a', time());
		$data['last_update_user'] = "aar";
		$data['last_update_url'] = "approve";
		
		$this->load->view('show_category_for_approval_view', $data);
	}	
	
	public function show_post($id)
	{
		$this->load->helper(array('form', 'url', 'captcha', 'string'));
		$this->load->library(array('form_validation', 'blog_utils'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');
		
		$this->form_validation->set_rules('body', 'Body', 'required');
		
		$this->load->model('pmodel');
		$data['post'] = $this->pmodel->post_get($id);
		
		if(count($data['post']) > 0)
		{
			$data['category_name'] = $this->pmodel->category_get_name($data['post']['category']);
			$data['category_id'] = $data['post']['category'];
			$data['posts_count'] = $this->pmodel->posts_count($data['post']['category']);
			
			$data['uservalid'] = FALSE;
			$data = $this->blog_utils->get_session_user($data);
			
			if($data['uservalid'] == 0)
			{
				$this->form_validation->set_rules('captcha','Captcha','callback_validate_captcha');
			}
			
			if ($this->form_validation->run() == TRUE)
			{
				$this->form_validation->unset_field_data();
				
				if($data['uservalid'] == 0)
				{
					$this->session->unset_userdata('captcha');
					$this->session->unset_userdata('image');
					
					
					
				}
				
				$comment['type'] = 2;
				$comment['category'] = $data['category_id']; 
				$comment['title'] = "comment"; 
				$comment['body'] =  $this->input->post('body'); 
				
				if($data['uservalid'])
				{
					$comment['author'] = $data['usersession'];
				}
				else
				{
					$comment['author'] = "anonymous";
				}
				
				$comment['parent_id'] = $data['post']['id'];
				
				$this->pmodel->post_add_comment($comment);
				
				$data['commented'] = TRUE;
			}
			
			if($data['uservalid'] == 0)
			{
				$random_numeric = random_string('numeric', 4);
				
				$vals = array(
					'word'	=> $random_numeric,
					'img_path'	=> './images/captcha/',
					'img_url'	=> base_url().'images/captcha/',
					'font_path'	=> './system/fonts/texb.ttf',
					'img_width'	=> '150',
					'img_height' => 30,
					'expiration' => 7200
					);
					
				$cap = create_captcha($vals);
				$data['image'] = $cap['image'];
				$this->session->set_userdata(array('captcha'=>$random_numeric, 'image' => $cap['time'].'.jpg'));
			}
			
			$data['post_comments'] = $this->pmodel->post_get_comments($data['post']['id']);
		}
		
		$data['uservalid'] = FALSE;
		$data = $this->blog_utils->get_session_user($data);
		
		$this->load->view('show_post_view', $data);
	}
	
	public function new_post($category)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('form_validation', 'blog_utils'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');

		$this->form_validation->set_rules('title', 'Title', 'required|max_length[511]');
		$this->form_validation->set_rules('body', 'Body', 'required');

		
		$this->load->model('pmodel');
		$data['category_name'] = $this->pmodel->category_get_name($category);
		
		$data['category_id'] = $category;
		
		$data['uservalid'] = FALSE;
		$data = $this->blog_utils->get_session_user($data);
		
		if ($this->form_validation->run() == TRUE)
		{
			$this->form_validation->unset_field_data();
			
			$post['type'] = 1;
			$post['category'] = $category; 
			$post['title'] = $this->input->post('title'); 
			$post['body'] =  $this->input->post('body'); 
			$post['author'] = $data['usersession']; 
			$post['parent_id'] = 0;
			
			if($data['uservalid'])
			{
				if($data['usersession'] == 'admin')
				{
					$post['approved'] = 1;
				}
			}
			else
			{
				$post['approved'] = 0;
			}

			$this->pmodel->post_new($post);
			
			redirect('/posts/show_category/'.$category);
		}
		else
		{
			$this->load->view('post_new_view', $data);
		}
	}
	
	public function edit_post($postid)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('form_validation', 'blog_utils'));
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div><br>');

		$this->form_validation->set_rules('title', 'Title', 'required|max_length[511]');
		$this->form_validation->set_rules('body', 'Body', 'required');

		
		$this->load->model('pmodel');
		$post = $this->pmodel->post_get($postid);
		
		if($post['id'])
		{
			$data['postid'] = $post['id'];
			if (isset($_POST['body']) OR isset($_POST['title']))
			{
				$data['post_title'] = $this->input->post('title');
				$data['post_body'] = $this->input->post('body');
			}
			else
			{
				$data['post_title'] = $post['title'];
				$data['post_body'] = $post['body'];
			}
			
			$data['category_name'] = $this->pmodel->category_get_name($post['category']);
			
			$data['category_id'] = $post['category'];
			
			$data['uservalid'] = FALSE;
			$data = $this->blog_utils->get_session_user($data);
			
			if ($this->form_validation->run() == TRUE)
			{
				$this->form_validation->unset_field_data();
				
				$post['title'] = $this->input->post('title');
				$post['body'] =  $this->input->post('body');
				
				if($data['uservalid'])
				{
					if($data['usersession'] == 'admin')
					{
						$post['approved'] = 1;
					}
				}
				else
				{
					$post['approved'] = 0;
				}

				$this->pmodel->post_edit($post);
				
				redirect('/posts/show_post/'.$postid);
			}
			else
			{
				$this->load->view('post_edit_view', $data);
			}
		}
	}
	
	public function approve_post($postid)
	{
		$this->load->model('pmodel');
		$this->pmodel->post_approve($postid);
		
		$this->show_post($postid);
	}
	
	public function delete_post($postid)
	{
		$this->load->model('pmodel');
		$post = $this->pmodel->post_get($postid);
		
		if($post['category'])
		{
			$this->pmodel->post_delete($postid);
			$this->show_category($post['category']);
		}
		else
		{
			$this->show_post($postid);
		}			
	}

	public function validate_captcha()
	{
		if (isset($_POST['captcha']))
		if($this->input->post('captcha') != $this->session->userdata['captcha'])
		{
			$this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
			return false;
		}
		else
		{
			return true;
		}

	}
	
}
