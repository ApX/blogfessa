<?php

$header['title'] = "User Show";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">User <?=$username?></h1>
        <p class="lead blog-description">User details provided in the form below. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
	  <table><tr><td width="15%">&nbsp;</td><td>
	  <blockquote>
			<table>
			<tr><td>Age:&nbsp;</td><td><?=$age;?></td></tr>
			<tr><td>About:&nbsp;</td><td><?=$about;?></td></tr>
			</table>
	  </blockquote></tr></table>
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
