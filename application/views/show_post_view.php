<?php

if(count($post)>0)
	$header['title'] = $category_name." / ".$post['title'];
else
	$header['title'] = "Page 404";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 0;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="row">
        <div class="col-sm-8 blog-main">

          <div class="blog-header">
		  <?php if(count($post)>0) : ?>
			<table></tr>
            <td><h1 class="blog-title"><?=$category_name;?> Category</h1></td>
			<td>&nbsp;</td>
			<td valign="bottom"><h6>[<a href="<?=base_url().'posts/show_category/'.$category_id;?>"><?=$posts_count;?> post(s)</a>] <a href="<?=base_url().'posts/new_post/'.$category_id;?>">new post</a></h6></td>
			</tr></table>
			<br>

					<pre><?php if($post['approved'] == 0) : ?><table border="0" width="100%"><tr><td style="color:red" align="right" valign="top" id="status">not approved...</td></tr></table><?php endif;?><font size="5"><?=$post['title'];?></font>
					<table border="0" width="100%">
						<tr><td width="2%">&nbsp;</td>
							<td valign="top" colspan="6"><code><?=$post['body'];?></code></td>
						</tr>
						<tr><td colspan="7">&nbsp;</td></tr>
						<tr><td width="2%">&nbsp;</td>
							<td width="30%"><code>by <a href="<?=base_url().'users/user_show/'.$post['author'];?>"><?=$post['author'];?></a></code></td>
							<td width="30%"><code>on <?=$post['date']?></code></td>
							<td><code><a href="#" id="post_comment" name="post_comment">comment</a></code></td>
							<?php if($uservalid) :?>
							<?php if(($usersession == $post['author']) or ($usersession == 'admin')) : ?>
							<td><code><a href="<?=base_url().'posts/edit_post/'.$post['id'];?>">edit</a></code></td>
							<td><code><a href="<?=base_url().'posts/delete_post/'.$post['id'];?>">delete</a></code></td>
							<?php endif;?>
							<?php endif;?>
							<?php if($uservalid) :?>
							<?php if($usersession == 'admin' and $post['approved'] == 0) : ?>
							<td><code><a href="<?=base_url().'posts/approve_post/'.$post['id'];?>">approve</a></code></td>
							<?php endif;?>
							<?php endif;?>

						</tr></table></pre>
						
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<?php foreach($post_comments as $comment):?>
						<tr>
							<td width="3%">&nbsp;</td>
							<td valign="top">
								<blockquote>
									<table border="0" width="100%">
										<tr><td valign="top" align="left"><font size="2">by <a href="<?=base_url().'users/user_show/'.$comment['author'];?>"><?=$comment['author'];?></a> on <?=$comment['date'];?></font></td></tr>
										<tr><td><?=$comment['body'];?></td></tr>
									</table>
								</blockquote>
							</td>
						</tr>
					<?php endforeach;?>
					</table>
					
					<hr>
					
					<?php echo validation_errors(); ?>
					<?php echo form_open('posts/show_post/'.$post['id']); ?>
						<table border="0" width="100%">
						<tr><td colspan="2"><textarea name="body" id="body"><?php echo set_value('body'); ?></textarea></td></tr>
						<?php if($uservalid == 0) : ?>
						<tr><td width="25%"><?=$image;?></td><td>Verfication: <input type="number" name="captcha"></input></td></tr>
						<?php endif;?>
						<tr><td colspan="2"><input type="submit" value="Comment!" name="comment" id="comment"></input></td></tr>
						</table>
					</form>
						
		<?php else : ?>
			<h1 class="blog-title">Page 404</h1>
		<?php endif;?>
        </div><!-- /.blog-main -->
      </div><!-- /.row -->

    </div><!-- /.container -->

	<?php if(isset($commented)) : ?>
	<?php if($commented) : ?>
		<script>
			document.body.scrollTop = $('#comment').offset().top;
			$('html,body').animate({scrollTop: $('#comment').offset().top});
		</script>
	<?php endif;?>
	<?php endif;?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	<script>
	$(function(){
      function go_to_comment(event) {
			document.body.scrollTop = $('#comment').offset().top;
			$('html,body').animate({scrollTop: $('#comment').offset().top});
			tinymce.execCommand('mceFocus',false,'body');
            return false;
      }
	$("#post_comment").click(go_to_comment);
	});
	</script>
  </body>
</html>
