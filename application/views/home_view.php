<?php

$header['title'] = "Home";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 1;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="row">
        <div class="col-sm-8 blog-main">

          <div class="blog-header">
            <h1 class="blog-title">Posts Categories</h1>
            <p class="blog-post-meta">Last <a href="<?=base_url().''.$last_update_url;?>">updated</a> by <a href="<?=base_url().'users/user_show/'.$last_update_user;?>"><?=$last_update_user;?></a> on <?=$last_update_time;?></p>
			
				<?php foreach($c_by_posts as $item):?>
					<pre><table border="0" width="100%">
						<tr>
						<?php if($item['count'] > 0) : ?>
							<td width="50%"><code><a href="<?=base_url().'posts/show_category/'.$item['id']?>"><?=$item['category']?></a></code></td>
						<?php else : ?>
							<td width="50%"><code><?=$item['category']?></code></td>
						<?php endif?>
							<td width="20%"><code>count: <?=$item['count']?></code></td>
							<td><code><a href="<?=base_url().'posts/new_post/'.$item['id']?>">new post</a></code></td>
						</tr></table></pre>
				<?php endforeach;?>

        </div><!-- /.blog-main -->
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
