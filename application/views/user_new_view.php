<?php

$header['title'] = "New User Registration";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 2;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">New User Registration Form</h1>
        <p class="lead blog-description">The registration form to add a new user to the system. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
		<?php echo validation_errors(); ?>
		<?php echo form_open('users/user_new'); ?>
			<table>
			<tr><td>User</td><td><input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>"></input></td></tr>
			<tr><td>Password</td><td><input type="password" name="password"></input></td></tr>
			<tr><td>Age</td><td><input type="number" name="age" value="<?php echo set_value('age'); ?>"></input></td></tr>
			<tr><td>E-mail</td><td><input type="text" name="email" value="<?php echo set_value('email'); ?>"></input></td></tr>
			<tr><td>About</td><td><textarea rows="5" cols="40" name="about"><?php echo set_value('about'); ?></textarea></td></tr>
			<tr><td>Verification&nbsp;</td><td><?=$image;?></td></tr>
			<tr><td>&nbsp;</td><td><input type="number" name="captcha"></input></td></tr>
			<tr><td>&nbsp;</td><td><input type="submit" value="Submit"></input></td></tr>
			</table>
		</form>

      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	<script>
		$("#username").focus();
	</script>
  </body>
</html>
