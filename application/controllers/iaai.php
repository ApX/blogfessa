<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iaai extends CI_Controller {
	
	public $main_url = "https://www.iaai.com/Vehicles/";
	public $img_main_url = "https://www.iaai.com/Images/EnlargeImages?stockNumber=";
	public $img_url = "https://vis.iaai.com/resizer?imageKeys=";

	
	function show_db()
	{
		$this->load->model('iaaimodel');
		echo $this->iaaimodel->count_all()."\r\n";
	}
	
	function get_cars($from,$to)
	{
		echo "<PRE>";
		
		//error_reporting(E_ALL);
		
		set_time_limit(0);
		
		
		ob_end_flush();
		ob_start();
		
		echo "<u>Getting the cars </u>\r\n\r\n";
		
		echo "From:\t".$from;
		echo "\r\n";
		echo "To:\t".$to."\r\n\r\n";
		
		ob_flush();
		flush();

		for($i=$from;$i<=$to;$i++)
		{
			$this->get_cars_keys($i);
			$this->get_cars_data($i);
			$this->get_cars_images($i);
		}
	}
	
	function get_cars_data($year)
	{
		$this->load->model('iaaimodel');
		
		$cars_urls = $this->iaaimodel->get_cars_urls_for_year($year);
		
		$i = 0;
		
		ob_end_flush();
		ob_start();

		
		foreach($cars_urls as $url)
		{
			$i++;
			
			$car_data = $this->get_car_data($url['lot_url']);
			
			if(strlen($car_data['lot_number']))
			{
				$this->iaaimodel->update_item($car_data);
			}
			
			echo "<div style='position:static;z-index:1;background-color:white'><ul><li>Getting cars...</li><li>".$this->iaaimodel->count_all()." row(s) inserted / Year: ".$year." / ".$i." / ".count($cars_urls)."	             </li><li></li></ul></div>";
		
			ob_flush();
			flush();
		}
	}
	
	function get_car_data($url)
	{
		$this->load->library('simple_html_dom');
		
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$error_i = 0;
		
		exec1:
		$result = curl_exec($curl);
		
		if(curl_errno($curl)){
			echo 'Curl error: ' . curl_errno($curl)."\r\n";
			if($error_i < 11)
			{
				$error_i++;
				sleep(3);
				goto exec1;
			}
		}
		
		curl_close($curl);
		
		$html = str_get_html($result);
		
		$result = null;
		
		$lot_number = $html->find('#ctl00_ContentPlaceHolder1_stockNumber', 0);
		
		if($lot_number)
		{
			if(strlen($lot_number->innertext))
			{
				$car_data['lot_number'] = $lot_number->innertext;
				
				$car_data['make'] = $html->find('#ctl00_ContentPlaceHolder1_make', 0)->innertext;
				$car_data['model'] = $html->find('#ctl00_ContentPlaceHolder1_model',0)->innertext;
				
				$vin_src = $html->find('#ctl00_ContentPlaceHolder1_fraCarsArrive',0)->src;
				preg_match('/^.*vin=([a-zA-Z0-9]*).origin_zip=.*/', $vin_src, $vin);
				
				$car_data['vin'] = $vin[1];
				
				$car_data['sale_status'] = $html->find('#ctl00_ContentPlaceHolder1_VehicleStatus',0)->innertext;
				$car_data['location'] = $html->find('#ctl00_ContentPlaceHolder1_location',0)->plaintext;
				
				foreach($html->find('.vehicle-stats') as $stats)
				{
					if(preg_match('/^.*Odometer.*span.([0-9]*)..span.*/', $stats->innertext, $odometer))
					{
						$car_data['odometer_reading'] = $odometer[1];
					}
					
					if(preg_match('/^.*Loss:..span...dt..dd..span.(.*)..span...dd...li..li..dt..span.Primary.*/', $stats->innertext, $damage))
					{
						$car_data['damage_description'] = $damage[1];
					}
					
					if(preg_match('/^.*Cylinders:..span...dt..dd..span.(.*)..span...dd...li..li..dt..span.Engine.*/', $stats->innertext, $cylinders))
					{
						$car_data['cylinders'] = $cylinders[1];
					}
					
					if(preg_match('/^.*Estimated Repair Cost:..span...dt..dd..span.(.*)..span...dd...li..li..dt..span.Radio.*/', $stats->innertext, $repair_estimate))
					{
						$car_data['repair_estimate'] = $repair_estimate[1];
					}
				}					
				
				$est_retail_value = $html->find('#ctl00_ContentPlaceHolder1_titleACV',0);
				if($est_retail_value)
				{
					$car_data['est_retail_value'] = $est_retail_value->innertext;
				}
			}
		}
		
		$car_data['lot_url'] = $url;
		
		return $car_data;
	}
	
	function get_cars_images($year)
	{
		$this->load->model('iaaimodel');
		
		$cars_lots = $this->iaaimodel->get_cars_lots_for_year($year);
		
		$i = 0;
		
		ob_end_flush();
		ob_start();

		foreach($cars_lots as $lot)
		{
			$i++;
			
			$car_imgs = $this->get_car_images($lot['lot_number']);
			
			if(count($car_imgs[1]))
			{
				$this->iaaimodel->delete_img($lot['id']);
				
				for($ii=0;$ii<count($car_imgs[1]);$ii++)
				{
					$img_data['res_id'] = $lot['id'];
					$img_data['url'] = $this->img_url.$car_imgs[1][$ii]."&width=".$car_imgs[4][0]."&height=".$car_imgs[4][1];
					
					$this->iaaimodel->insert_img_item($img_data);
				}
			}
			
			echo "<div style='position:static;z-index:1;background-color:white'><ul><li>Getting cars images...</li><li>".$this->iaaimodel->count_all()." row(s) inserted / Year: ".$year." / ".$i." / ".count($cars_lots)."	             </li><li></li></ul></div>";
			
			ob_flush();
			flush();
		}
	}
	
	function get_car_images($lot_number)
	{
		$curl = curl_init($this->img_main_url.$lot_number);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$error_i = 0;
		
		exec2:
		$result = curl_exec($curl);
		
		if(curl_errno($curl)){
			echo 'Curl error: ' . curl_errno($curl)."\r\n";
			if($error_i < 11)
			{
				$error_i++;
				sleep(3);
				goto exec2;
			}
		}
		
		curl_close($curl);
		

		//var width = "640";
		//var height = "480";
		preg_match('/var width = "([0-9]*)";/', $result, $width);
		preg_match('/var height = "([0-9]*)";/', $result, $height);
		
		//"keys":[{"K":"16477640~SID~B731~S0~I1~RW640~H480~TH0","W":640,"H":480,"SN":
		preg_match_all('/{"K":"([A-Z0-9~a-z]*)","W":[0-9]*,"H":[0-9]*,"SN":/', $result, $images_paths);
		
		$result = null;
		
		$images_paths[4][0] = $width[1];
		$images_paths[4][1] = $height[1];
		
		return $images_paths;
	}
	
	function get_cars_keys($year)
	{
		$url = $this->main_url."Search.aspx?Keyword=automobiles+".$year;
		
		$this->load->model('iaaimodel');
		
		$this->load->library('simple_html_dom');

		/********************************************************FIRST CALL*/
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		$result = curl_exec($curl);
		
		$error_i = 0;
		
		exec3:
		$info = curl_getinfo($curl);
		
		if(curl_errno($curl)){
			echo 'Curl error: ' . curl_errno($curl)."\r\n";
			if($error_i < 11)
			{
				$error_i++;
				sleep(3);
				goto exec3;
			}
		}
		
		curl_close($curl);
		
		preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $main_cookies);
		
		$html = str_get_html($result);

		$__VIEWSTATE = $html->getElementById('__VIEWSTATE');
		$__EVENTVALIDATION = $html->getElementById('__EVENTVALIDATION');
		$__VIEWSTATEGENERATOR = $html->getElementById('__VIEWSTATEGENERATOR');

		$total = 0;

		$__VIEWSTATE_cur = $__VIEWSTATE->value;
		$__EVENTVALIDATION_cur = $__EVENTVALIDATION->value;
		$__VIEWSTATEGENERATOR_cur = $__VIEWSTATEGENERATOR->value;
		/********************************************************FIRST CALL end*/
		
		$loaded = 0;
		
		$item = null;
		
		foreach($html->find('a.stocklink') as $element) 
		{
			
			$item['lot_url'] = $this->main_url.$element->href;
			$item['lot_number'] = $element->innertext;
			
			$item['est_retail_value'] = " ";
			$item['expected_sale_date'] = " ";
			$item['year'] = $year;
			$item['make'] = " ";
			$item['model'] = " ";
			$item['current_bid'] = 0;
			
			$exists_lot = $this->iaaimodel->exists_lot($item['lot_number']);
			
			if($exists_lot == 0)
			{
				$this->iaaimodel->insert_item($item);
			}

			$loaded++;
			$total++;
		}
		
		ob_end_flush();
		ob_start();

		if($loaded)
		{
			$current_page = $html->getElementById('ctl00_ContentPlaceHolder1_Pager_lblCurrentPage')->innertext;
			echo "<div style='position:static;z-index:1;background-color:white'><ul><li>Getting keys...</li><li>".$this->iaaimodel->count_all()." row(s) inserted / Year: ".$year." / ".strlen($result)." / ".$total."</li><li>".$current_page."</li><li>".$info['total_time']."</li></ul></div>";

		}
		
		ob_flush();
		flush();

		if($loaded)
		{
			$next = $html->find('#ctl00_ContentPlaceHolder1_Pager_NextPageLinkButton', 0);
			
			if($next->disabled)
			{
				$loaded = 0;
			}
		}
		
		$html = null;
		$result = null;
		
		while($loaded > 0)
		{
			$loaded = 0;
			
			$postdata = http_build_query(array('__VIEWSTATE'=> $__VIEWSTATE_cur,'__VIEWSTATEGENERATOR'=>$__VIEWSTATEGENERATOR_cur,'__EVENTVALIDATION'=>$__EVENTVALIDATION_cur,'__EVENTTARGET'=>"ctl00\$ContentPlaceHolder1\$Pager\$NextPageLinkButton"));
			
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_VERBOSE, 1);

			$all_cookies = "";
			$cookies_i = 0;
			
			$item = null;
			
			foreach($main_cookies[1] as $item)
			{
				if($cookies_i>0)
				{
					$all_cookies = $all_cookies.$item.";";
				}
				else
				{
					$all_cookies = $item.";";			
				}
				
				$cookies_i++;
			}
			
			curl_setopt($curl, CURLOPT_COOKIE, $all_cookies);
			
			$result = curl_exec($curl);
			
			$error_i = 0;
			
			exec4:
			$info = curl_getinfo($curl);
			
			if(curl_errno($curl)){
				echo 'Curl error: ' . curl_errno($curl)."\r\n";
				if($error_i < 11)
				{
					$error_i++;
					sleep(3);
					goto exec4;
				}
			}
			
			curl_close($curl);
			
			if(strlen($result) < 1)
			{
				$loaded = 1;
				continue;
			}
			
			$html = str_get_html($result);
			
			$__VIEWSTATE = $html->getElementById('__VIEWSTATE');
			$__EVENTVALIDATION = $html->getElementById('__EVENTVALIDATION');
			$__VIEWSTATEGENERATOR = $html->getElementById('__VIEWSTATEGENERATOR');

			$__VIEWSTATE_cur = $__VIEWSTATE->value;
			$__EVENTVALIDATION_cur = $__EVENTVALIDATION->value;
			$__VIEWSTATEGENERATOR_cur = $__VIEWSTATEGENERATOR->value;
			
			$__VIEWSTATE = null;
			$__EVENTVALIDATION = null;
			$__VIEWSTATEGENERATOR = null;
			
			$item = null;

			foreach($html->find('a.stocklink') as $element) 
			{
				$item['lot_url'] = $this->main_url.$element->href;
				$item['lot_number'] = $element->innertext;
				
				$item['est_retail_value'] = " ";
				$item['expected_sale_date'] = " ";
				$item['year'] = $year;
				$item['make'] = " ";
				$item['model'] = " ";
				$item['current_bid'] = 0;

				$exists_lot = $this->iaaimodel->exists_lot($item['lot_number']);
				
				if($exists_lot == 0)
				{
					$this->iaaimodel->insert_item($item);
				}
				
				$loaded++;
				$total++;
			}
			
			ob_end_flush();
			ob_start();

			if($loaded)
			{
				$current_page = $html->getElementById('ctl00_ContentPlaceHolder1_Pager_lblCurrentPage')->innertext;
				echo "<div style='position:static;z-index:1;background-color:white'><ul><li>Getting keys...</li><li>".$this->iaaimodel->count_all()." row(s) inserted / Year: ".$year." / ".strlen($result)." / ".$total."</li><li>".$current_page."</li><li>".$info['total_time']."</li></ul></div>";
			}
			
			ob_flush();
			flush();
			
			if($loaded)
			{
				$next = $html->find('#ctl00_ContentPlaceHolder1_Pager_NextPageLinkButton', 0);
				
				if($next->disabled)
				{
					$loaded = 0;
				}
			}
			
			$html = null;
			$result = null;
		}
	}
}