<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pmodel extends CI_Model {
	function categories_by_posts()
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "c.id = p.category AND p.approved = 1";
		
		if($data['uservalid'])
		{
			$where = "c.id = p.category AND (p.approved = 1 OR p.author = '".$data['usersession']."')";
			if($data['usersession'] == 'admin')
			{
				$where = "c.id = p.category";
			}
		}
		
		$this->db->select('c.id, c.category, COUNT(p.category) as "count"', FALSE);
		$this->db->from('categories c');
		$this->db->join('posts p', $where, 'left');
		$this->db->group_by('c.id, c.category'); 
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	function categories_by_posts_for_approval()
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "c.id = p.category AND p.approved = 2";
		
		if($data['uservalid'])
		{
			if($data['usersession'] == 'admin')
			{
				$where = "c.id = p.category AND p.approved = 0";
			}
		}
		
		$this->db->select('c.id, c.category, COUNT(p.category) as "count"', FALSE);
		$this->db->from('categories c');
		$this->db->join('posts p', $where, 'left');
		$this->db->group_by('c.id, c.category'); 
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	function category_get($category)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "p.approved = 1";
		
		if($data['uservalid'])
		{
			$where = "(p.approved = 1 OR p.author = '".$data['usersession']."')";
			if($data['usersession'] == 'admin')
			{
				$where = "p.approved >= 0";
			}
		}

		$this->db->select('p.*', FALSE);
		$this->db->from('posts p');
		$this->db->where('p.category', $category);
		$this->db->where($where);
		$this->db->order_by('p.id');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	function category_for_approval_get($category)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "p.approved = 2";
		
		if($data['uservalid'])
		{
			if($data['usersession'] == 'admin')
			{
				$where = "p.approved = 0";
			}
		}

		$this->db->select('p.*', FALSE);
		$this->db->from('posts p');
		$this->db->where('p.category', $category);
		$this->db->where($where);
		$this->db->order_by('p.id');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	function category_get_name($category)
	{
		$this->db->select('category');
		$this->db->where('id', $category);
		
		$query = $this->db->get('categories');
		
		return $query->row()->category;
	}
	
	function post_get($id)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "p.approved = 1";
		
		if($data['uservalid'])
		{
			$where = "(p.approved = 1 OR p.author = '".$data['usersession']."')";
			if($data['usersession'] == 'admin')
			{
				$where = "p.approved >= 0";
			}
		}
		
		$this->db->where('id', $id);
		$this->db->where($where);
		$query = $this->db->get('posts p');
		
		return $query->row_array();
	}
	
	function post_new($post)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);

		if($data['uservalid'])
		{
			if($data['usersession'] == $post['author'])
			{
				$this->db->insert('posts',$post);
			}
		}
	}
	
	function post_edit($post)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);

		if($data['uservalid'])
		{
			if($data['usersession'] == $post['author'] OR $data['usersession'] == 'admin')
			{
				$this->db->where('id', $post['id']);
				$this->db->update('posts',$post);
			}
		}
	}
	
	function post_delete($postid)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);

		if($data['uservalid'])
		{
			$post = $this->post_get($postid);
			if($data['usersession'] == $post['author'] OR $data['usersession'] == 'admin')
			{
				$this->db->where('id', $postid);
				$this->db->delete('posts');
			}
		}
	}
	
	function post_approve($postid)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		if($data['uservalid'])
		{
			if($data['usersession'] == 'admin')
			{
				$this->db->where('id', $postid);
				$this->db->set('approved', 1);
				$this->db->update('posts');
			}
		}
	}
	
	function posts_count($category)
	{
		$this->load->library('blog_utils');
		
		$data['uservalid'] = 0;
		$data = $this->blog_utils->get_session_user($data);
		
		$where = "p.approved = 1";
		
		if($data['uservalid'])
		{
			$where = "(p.approved = 1 OR p.author = '".$data['usersession']."')";
			if($data['usersession'] == 'admin')
			{
				$where = "p.approved >= 0";
			}
		}

		$this->db->where('category', $category);
		$this->db->where($where);
		$this->db->from('posts p');
		return $this->db->count_all_results();
	}
	
	function post_get_comments($postid)
	{
		$this->db->where('parent_id', $postid);
		$query = $this->db->get('posts_comments');
		
		return $query->result_array();
	}
	
	function post_add_comment($comment)
	{
		$this->db->insert('posts_comments', $comment);
	}
}
