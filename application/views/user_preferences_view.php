<?php

$header['title'] = "User Preferences";

if(isset($uservalid))
{
	if($uservalid)
	{
		$header['uservalid'] = $uservalid;
		$header['usersession'] = $usersession;
	}
}
else
{
	$header['uservalid'] = 0;
}

$header['active_menu'] = 4;
$header['user_added'] = 0;
$header['pref_updates'] = 0;

$this->load->view('header_view', $header);

?>

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">User Preferences Form</h1>
        <p class="lead blog-description">You can change your preferences. [<?php echo date('d-M-Y h:i:s a', time());?>]</p>
      </div>

      <div class="row">
		<?php if($uservalid) : ?>
		<?php echo validation_errors(); ?>
		<?php echo form_open('users/user_preferences'); ?>
			<table>
			<tr><td>Password</td><td><input type="password" name="password"></input></td></tr>
			<tr><td>Age</td><td><input type="number" name="age" value="<?=$age;?>"></input></td></tr>
			<tr><td>E-mail</td><td><input type="text" name="email" value="<?=$email;?>"></input></td></tr>
			<tr><td>About</td><td><textarea rows="5" cols="40" name="about"><?=$about;?></textarea></td></tr>
			<tr><td>&nbsp;</td><td><input type="submit" value="Submit"></input></td></tr>
			</table>
		</form>
		<?php else : ?>
		<table><tr><td width="15%">&nbsp;</td><td>
		  <blockquote>
				<table>
				<tr><td>You are not logged in.</td></tr>
				<tr><td>To authenticate use <a href="<?=base_url();?>users/user_login">Login</a>.</td></tr>
				</table>
		  </blockquote></td></tr></table>
		<?php endif;?>
      </div><!-- /.row -->

    </div><!-- /.container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
